package spotify_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.HomePage;
import pages.ProfilePage;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class BaseTest {
    private WebDriver driver;
    private static final String SPOTIFY_URL = "https://accounts.spotify.com/ru/login";

    @BeforeTest
    public void profileSetUp() {
        chromedriver().setup();
    }

    @BeforeMethod
    public void testsSetUp() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(SPOTIFY_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public ProfilePage getProfilePage() {
        return new ProfilePage(driver);
    }

}
