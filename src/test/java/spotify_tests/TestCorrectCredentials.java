package spotify_tests;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.ProfilePage;

public class TestCorrectCredentials extends BaseTest {
    @Test
    @Parameters({"email", "password", "expectedResult"})
    public void loginInputWithIncorrectCredentials(String email, String password, String expectedResult) {
        HomePage homePage = getHomePage();
        homePage.implicitWait(10);
        homePage.inputLogin(email);
        homePage.inputPassword(password);
        homePage.clickInnerButton();
        homePage.implicitWait(30);
        ProfilePage profilePage = getProfilePage();
        Assert.assertEquals(profilePage.getProfileName(), expectedResult);
    }
}
