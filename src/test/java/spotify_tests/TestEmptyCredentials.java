package spotify_tests;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.HomePage;

public class TestEmptyCredentials extends BaseTest {
    @Test
    @Parameters({"email", "expectedResult"})
    public void loginInputWithEmptyCredentials(String email, String expectedResult) {
        HomePage homePage = getHomePage();
        homePage.implicitWait(10);
        homePage.inputLogin(email);
        homePage.clearLogin();
        Assert.assertEquals(homePage.getLoginErrorMessage(), expectedResult);
    }

    @Test
    @Parameters({"password", "expectedResult"})
    public void passwordInputWithEmptyCredentials(String password, String expectedResult) {
        HomePage homePage = getHomePage();
        homePage.implicitWait(10);
        homePage.inputPassword(password);
        homePage.clearPassword();
        Assert.assertEquals(homePage.getPasswordErrorMessage(), expectedResult);
    }
}
