package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProfilePage extends BasePage {

    @FindBy(xpath = "//div[@class='sc-gswNZR sc-dkrFOg wKwWn fKGPYu']")
    private WebElement profileName;

    public String getProfileName() {
        return profileName.getText();
    }

    public ProfilePage(WebDriver driver) {
        super(driver);
    }
}
