package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//button[@data-testid='login-button']")
    private WebElement innerButton;

    @FindBy(xpath = "//input[@id='login-username']")
    private WebElement inputUsername;

    @FindBy(xpath = "//div[@data-testid='username-error']")
    private WebElement loginErrorMessage;

    @FindBy(xpath = "//input[@id='login-password']")
    private WebElement inputPassword;

    @FindBy(xpath = "//div[@data-testid='password-error']")
    private WebElement passwordErrorMessage;

    @FindBy(xpath = "//div[@data-encore-id='banner']")
    private WebElement messageErrorText;

    public String getMessageErrorText() {
        return messageErrorText.getText();
    }

    public void inputLogin(String email) {
        inputUsername.sendKeys(email);
    }

    public void clearLogin() {
        inputUsername.sendKeys(Keys.chord(Keys.COMMAND, "a"));
        inputUsername.sendKeys(Keys.DELETE);
    }

    public void inputPassword(String password) {
        inputPassword.sendKeys(password);
    }

    public void clearPassword() {
        inputPassword.sendKeys(Keys.chord(Keys.COMMAND, "a"));
        inputPassword.sendKeys(Keys.DELETE);
    }

    public String getLoginErrorMessage() {
        return loginErrorMessage.getText();
    }

    public String getPasswordErrorMessage() {
        return passwordErrorMessage.getText();
    }


    public void clickInnerButton() {
        innerButton.click();
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }
}
